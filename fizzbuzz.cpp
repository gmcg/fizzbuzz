//
// its fizzbuzz my dudes 
// gregory mcguire wrote this on may 14th, 2019
// https://gitlab.com/gmcg
// 
// this makes no use of the cool stuff C++ has to offer
// if it weren't for cout this would be at best C code
// 
// [insert MIT license here]
// 

/*
from wikipedia
Players take turns to count incrementally, replacing any number divisible by three 
with the word "fizz", and any number divisible by five with the word "buzz".

Numbers divisible by both become fizz buzz.
*/

#include <iostream>

using namespace std;

int main()
{
	
	const int GOAL = 100;
	int x = 0;
	
	// make sure i remembered how to set constants right
	//cout << GOAL;
	
	for (x = 1; x < GOAL + 1; ++x){ // spaghetti code LUL
		
		if(x%3==0 && x%5==0){
			
				cout << "fizzbuzz ";
				//exit;
				//all these exit;s do nothing
			}
		if(x%3==0){
			
				cout << "fizz ";
				//exit;
			}
		if(x%5==0){
			
				cout << "buzz ";
				//exit;
			}
		
		if (x%3!=0 && x%5!=0)
			cout << x << " ";
		}
		
	// spacing
	cout << endl;
	return 0;
	
}
